// https://webpack.js.org/configuration/

const webpack = require('webpack')

const config = {
    entry: [
        'script-loader!jquery/dist/jquery.min.js',
        'script-loader!foundation-sites/dist/js/foundation.min.js',
        './app/app.jsx'
    ],
    externals: {
        jquery: 'jQuery'
    },
    plugins: [
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery'
        })
    ],
    output: {
        filename: './public/bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx'],
        modules: [
            __dirname + '/app/styles',
            __dirname + '/app/components',
            __dirname + '/app/api',
            'node_modules'
        ]
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                },
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/
            }
        ]
    },
    devtool: 'cheap-module-eval-source-map'
}

module.exports = config
