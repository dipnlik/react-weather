import React from 'react'
import { Link } from 'react-router-dom'

export default class Examples extends React.Component {
    render() {
        return (
            <div className="c-examples">
                <h1 className="text-center">Examples</h1>
                <p>Try these out!</p>
                <ul>
                    <li><Link to="/weather?location=Sorocaba">Sorocaba</Link></li>
                    <li><Link to="/weather?location=London">London</Link></li>
                </ul>
            </div>
        )
    }
}
