import React from 'react'

export default class WeatherMessage extends React.Component {
    render() {
        const { location, temp } = this.props
        return (
            <div className="c-weather-message">
                <p className="text-center">It's {temp}℃ in {location}.</p>
            </div>
        )
    }
}
