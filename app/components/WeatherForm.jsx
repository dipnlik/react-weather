import React from 'react'

export default class WeatherForm extends React.Component {
    onFormSubmit(event) {
        event.preventDefault()

        const location = this.refs.location.value
        if (location.length > 0) {
            this.refs.location.value = ''
            this.props.onSearch(location)
        }
    }

    render() {
        return (
            <div className="c-weather-form">
                <form action="/api/weather" onSubmit={this.onFormSubmit.bind(this)} >
                    <input type="search" ref="location" placeholder="City" />
                    <button className="button expanded">Get Weather</button>
                </form>
            </div>
        )
    }
}
