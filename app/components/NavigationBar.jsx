import React from 'react'
import { NavLink } from 'react-router-dom'

export default class NavigationBar extends React.Component {
    onSearch(event) {
        event.preventDefault()

        const location = this.refs.location.value
        if (location.length > 0) {
            this.refs.location.value = ""
            window.location.hash = "#/weather?location=" + encodeURIComponent(location)
        }
    }

    render() {
        return (
            <nav className="c-navigation-bar top-bar">
                <div className="top-bar-left">
                    <ul className="menu">
                        <li className="menu-text">React Weather</li>
                        <NavLinkListItem to="/weather" text="Get Weather" />
                        <NavLinkListItem to="/about" text="About" />
                        <NavLinkListItem to="/examples" text="Examples" />
                    </ul>
                </div>
                <div className="top-bar-right">
                    <form onSubmit={this.onSearch.bind(this)}>
                        <ul className="menu">
                            <li><input type="search" ref="location" placeholder="City" /></li>
                            <li><input type="submit" className="button" value="Get Weather" /></li>
                        </ul>
                    </form>
                </div>
            </nav>
        )
    }
}

// Not a great idea, but good for practice purposes
const NavLinkListItem = ({ to, text }) => (
    <li><NavLink to={to} activeClassName="active">{text}</NavLink></li>
)
