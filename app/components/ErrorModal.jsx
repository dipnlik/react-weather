import React from 'react'
import PropTypes from 'prop-types'

export default class ErrorModal extends React.Component {
    static get defaultProps() {
        return {
            title: "Oops :("
        }
    }

    static get propTypes() {
        return {
            title: PropTypes.string,
            message: PropTypes.string.isRequired
        }
    }

    componentDidMount() {
        $("#error-modal").foundation()
        $('#error-modal').foundation('open')

    }

    render() {
        const { title, message } = this.props
        return (
            <div className="c-error-modal">
                <div id="error-modal" className="reveal tiny" data-reveal="">
                    <h3>{title}</h3>
                    <p>{message}</p>
                    <button className="button expanded" data-close="">OK</button>
                </div>
            </div>
        )
    }
}
