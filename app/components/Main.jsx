import React from 'react'
import { Redirect, Route } from 'react-router-dom'

import NavigationBar from 'NavigationBar'
import Weather from 'Weather'
import About from 'About'
import Examples from 'Examples'

export default class Main extends React.Component {
    render() {
        return (
            <div className="c-main">
                <NavigationBar />
                <div className="grid-container grid-container-padded">
                    <div className="grid-x">
                        <div className="cell large-4 large-offset-4">
                            <Route exact path="/" render={() => (
                                <Redirect to="/weather" />
                            )} />
                            <Route path="/weather" component={Weather} />
                            <Route path="/about" component={About} />
                            <Route path="/examples" component={Examples} />
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}
