import React from 'react'
import WeatherForm from 'WeatherForm'
import WeatherMessage from 'WeatherMessage'
import ErrorModal from 'ErrorModal'
import openWeatherMap from 'openWeatherMap'

export default class Weather extends React.Component {
    constructor(props) {
        super(props)
        this.state = { isLoading: false }
    }

    componentDidMount() {
        const queryString = new URLSearchParams(this.props.location.search)
        const location = queryString.get('location')
        if (location && location.length > 0) {
            this.handleSearch(location)
        }
    }

    componentWillReceiveProps(newProps) {
        const queryString = new URLSearchParams(newProps.location.search)
        const location = queryString.get('location')
        if (location && location.length > 0) {
            this.handleSearch(location)
        }
    }

    handleSearch(location) {
        this.setState({ isLoading: true, errorMessage: null, location: null, temp: null })

        const parent = this;
        openWeatherMap.getTemp(location).then(
            (temp) => {
                parent.setState({ isLoading: false, location: location, temp: temp })
            },
            (error) => {
                parent.setState({ isLoading: false, errorMessage: error.message })
            }
        )
    }

    render() {
        const { isLoading, location, temp, errorMessage } = this.state

        function renderMessage() {
            if (isLoading) {
                return <p>Fetching weather…</p>
            } else if (typeof errorMessage === 'string') {
                return <ErrorModal message={errorMessage} />
            } else if (temp && location) {
                return <WeatherMessage location={location} temp={temp} />
            }
        }

        return (
            <div className="c-weather">
                <h1 className="text-center">Get Weather</h1>
                <WeatherForm onSearch={this.handleSearch.bind(this)} />
                {renderMessage()}
            </div>
        )
    }
}
