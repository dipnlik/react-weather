import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter as Router, Route } from 'react-router-dom'

import Main from 'Main'

require('style-loader!css-loader!foundation-sites/dist/css/foundation.min.css')
require('style-loader!css-loader!sass-loader!app.scss')

ReactDOM.render(
    <Router>
        <div className="c-app">
            <Route component={Main} />
        </div>
    </Router>,
    document.getElementById('app')
)
