import axios from 'axios'

const BASE_URI = 'https://api.openweathermap.org/data/2.5/weather?appid=3a03a61b9fcdd3f84df2c973545d3449&units=metric'

module.exports = {
    getTemp: function (location) {
        const encodedLocation = encodeURIComponent(location)
        const requestURI = `${BASE_URI}&q=${encodedLocation}`

        return axios.get(requestURI).then(
            (response) => {
                if (response.data.cod == 200) {
                    return response.data.main.temp
                } else {
                    throw new Error(response.data.message)
                }
            },
            (error) => {
                throw new Error(error.response.data.message)
            }
        )
    }
}
