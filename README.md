# react-weather

<https://dipnlik.gitlab.io/react-weather/>

## Notes

- `webpack`, `babel-*`, `*-loader`, `foundation-sites` and `jquery` used to be `devDependencies` but adding `bundle.js` to version control doesn't make sense to me since it can be built from the other files.

## References

- <https://walmart.udemy.com/the-complete-react-web-app-developer-course/>
- <https://docs.npmjs.com/misc/scripts>
- <https://devcenter.heroku.com/articles/nodejs-support#heroku-specific-build-steps>
- [devtool: cheap-module-eval-source-map](https://webpack.js.org/configuration/devtool/)
- <http://foundation.zurb.com/sites/docs/> (6.4.1)
